module Toto : sig

  type foobar =
    | Foo of int
    | Bar of string
  [@@deriving variants]

end = struct

  type foobar =
    | Foo of int
    | Bar of string
  [@@deriving variants]

end

include Toto
