(* Generated code should depend on the environment in scope as little as possible.
   E.g. rather than [foo = []] do [match foo with [] ->], to eliminate the use of [=].  It
   is especially important to not use polymorphic comparisons, since we are moving more
   and more to code that doesn't have them in scope. *)


open Base
open Ppxlib
open Ast_builder.Default

let raise_unsupported loc =
  Location.raise_errorf ~loc
    "Unsupported use of variants (you can only use it on variant types)."

module Create = struct
  let lambda loc xs body =
    List.fold_right xs ~init:body ~f:(fun (label, p) e -> pexp_fun ~loc label None p e)
  ;;

  let lambda_sig loc arg_tys body_ty =
    List.fold_right arg_tys ~init:body_ty ~f:(fun (label, arg_ty) acc ->
      ptyp_arrow ~loc label arg_ty acc)
  ;;
end

module Variant_constructor = struct
  type t = {
    name    : string;
    loc     : Location.t;
    kind    : [ `Normal of core_type list
              | `Polymorphic of core_type option ]
  }

  let args t =
    match t.kind with
    | `Normal pcd_args ->
      List.mapi pcd_args ~f:(fun i _ -> Nolabel, "v" ^ Int.to_string i)
    | `Polymorphic None -> []
    | `Polymorphic (Some _) -> [Nolabel, "v0"]

  let pattern_without_binding { name; loc; kind } =
    match kind with
    | `Normal [] ->
      ppat_construct ~loc (Located.lident ~loc name) None
    | `Normal (_ :: _) ->
      ppat_construct ~loc (Located.lident ~loc name) (Some (ppat_any ~loc))
    | `Polymorphic None ->
      ppat_variant ~loc name None
    | `Polymorphic (Some _) ->
      ppat_variant ~loc name (Some (ppat_any ~loc))

  let to_fun_type t ~rhs:body_ty =
    let arg_types =
      match t.kind with
      | `Polymorphic None -> []
      | `Polymorphic (Some v) -> [(Nolabel, v)]
      | `Normal args -> List.map args ~f:(fun typ -> Nolabel, typ)
    in
    Create.lambda_sig t.loc arg_types body_ty
end

let variant_name_to_string v =
  let s = String.lowercase v in
  if Caml.Hashtbl.mem Lexer.keyword_table s
  then s ^ "_"
  else s

module Inspect = struct
  let row_field loc rf : Variant_constructor.t =
    match rf.prf_desc with
    | Rtag ({ txt = name; _ }, true, _) | Rtag ({ txt = name; _ }, _, []) ->
      { name
      ; loc
      ; kind = `Polymorphic None
      }
    | Rtag ({ txt = name; _}, false, tp :: _) ->
      { name
      ; loc
      ; kind = `Polymorphic (Some tp)
      }
    | Rinherit _ ->
      Location.raise_errorf ~loc
        "ppx_variants_conv: polymorphic variant inclusion is not supported"

  let constructor cd : Variant_constructor.t =
    if Option.is_some cd.pcd_res then
      Location.raise_errorf ~loc:cd.pcd_loc "GADTs are not supported by variantslib";
    let kind =
      match cd.pcd_args with
      | Pcstr_tuple pcd_args -> `Normal pcd_args
      | Pcstr_record _  -> Location.raise_errorf ~loc:cd.pcd_loc "Inline records are not supported by variantslib"
    in
    { name = cd.pcd_name.txt
    ; loc = cd.pcd_name.loc
    ; kind
    }

  let type_decl td =
    let loc = td.ptype_loc in
    match td.ptype_kind with
    | Ptype_variant cds ->
      let cds = List.map cds ~f:constructor in
      let names_as_string = Hashtbl.create (module String) in
      List.iter cds ~f:(fun { name; loc; _ } ->
        let s = variant_name_to_string name in
        match Hashtbl.find names_as_string s with
        | None -> Hashtbl.add_exn names_as_string ~key:s ~data:name
        | Some name' ->
          Location.raise_errorf ~loc
            "ppx_variants_conv: constructors %S and %S both get mapped to value %S"
            name name' s
      );
      cds
    | Ptype_record _ | Ptype_open -> raise_unsupported loc
    | Ptype_abstract ->
      match td.ptype_manifest with
      | Some { ptyp_desc = Ptyp_variant (row_fields, Closed, None); _ } ->
        List.map row_fields ~f:(row_field loc)
      | Some { ptyp_desc = Ptyp_variant _; ptyp_loc = loc; _ } ->
        Location.raise_errorf ~loc
          "ppx_variants_conv: polymorphic variants with a row variable are not supported"
      | _ -> raise_unsupported loc
end

module Gen_sig = struct
  let apply_type loc ~ty_name ~tps =
    ptyp_constr ~loc (Located.lident ~loc ty_name) tps

  let label_arg _loc name ty =
    (Asttypes.Labelled (variant_name_to_string name), ty)
  ;;

  let val_ ~loc name type_ =
    psig_value ~loc (value_description ~loc ~name:(Located.mk ~loc name) ~type_ ~prim:[])
  ;;

  module V = Variant_constructor
  
  let variant ~variant_type loc variants =
    let constructors =
      List.map variants ~f:(
          fun v ->
          let constructor_type = V.to_fun_type v ~rhs:variant_type in
          let name = variant_name_to_string v.V.name in
          val_ ~loc name constructor_type
        )
    in
    constructors

  let constructor_type ~loc v = match v.V.kind with
    | `Polymorphic ty_opt -> (
        match ty_opt with
        | None -> [%type: unit]
        | Some ty -> ty
      )
    | `Normal tys -> (
        match tys with
        | [] -> [%type: unit]
        | tys -> ptyp_tuple ~loc tys
      ) 
  
  let matchings_exn ~variant_type loc variants =
    let matching_exn v =
      let uncapitalized = variant_name_to_string v.V.name in
      let match_exn_name = "get_" ^ uncapitalized ^ "_exn" in
      let ty = [%type: ([%t variant_type] -> [%t constructor_type ~loc v]) ] in
      val_ ~loc match_exn_name ty
    in
    List.map variants ~f:matching_exn

  let matchings_opt ~variant_type loc variants =
    let matching_opt v =
      let uncapitalized = variant_name_to_string v.V.name in
      let match_opt_name = "get_" ^ uncapitalized ^ "_opt" in
      let ty = [%type: ([%t variant_type] -> [%t constructor_type ~loc v] option) ] in
      val_ ~loc match_opt_name ty
    in
    List.map variants ~f:matching_opt

  let matchings_is ~variant_type loc variants =
    let matching_is v =
      let uncapitalized = variant_name_to_string v.V.name in
      let match_is_name = "is_" ^ uncapitalized in
      let ty = [%type: ([%t variant_type] -> bool) ] in
      val_ ~loc match_is_name ty
    in
    List.map variants ~f:matching_is

  let variants_of_td td =
    let ty_name = td.ptype_name.txt in
    let loc = td.ptype_loc in
    let variant_type = apply_type loc ~ty_name ~tps:(List.map td.ptype_params ~f:fst) in
    variant ~variant_type loc (Inspect.type_decl td)

  let matchings_of_td td =
    let ty_name = td.ptype_name.txt in
    let loc = td.ptype_loc in
    let variant_type = apply_type loc ~ty_name ~tps:(List.map td.ptype_params ~f:fst) in
    matchings_exn ~variant_type loc (Inspect.type_decl td) @
    matchings_opt ~variant_type loc (Inspect.type_decl td) @
    matchings_is ~variant_type loc (Inspect.type_decl td) 
  
  let generate ~loc ~path:_ (rec_flag, tds) =
    (match rec_flag with
     | Nonrecursive ->
       Location.raise_errorf ~loc
         "nonrec is not compatible with the `ppx_variants_conv' preprocessor"
     | _ -> ());
    match tds with
    | [td] -> variants_of_td td @ matchings_of_td td
    | _ -> Location.raise_errorf ~loc "multiple mutually recursive type declarations not supported"
end

module Gen_str = struct

  module V = Variant_constructor

  let constructors loc variants =
    let f v =
      let uncapitalized = variant_name_to_string v.V.name in
      let constructed_value =
        match v.V.kind with
        | `Normal _      ->
           let arg = pexp_tuple_opt ~loc (List.map (V.args v) ~f:(fun (_,v) -> evar ~loc v)) in
           pexp_construct ~loc (Located.lident ~loc v.V.name) arg
        | `Polymorphic _ ->
           let arg = pexp_tuple_opt ~loc (List.map (V.args v) ~f:(fun (_,v) -> evar ~loc v)) in
           pexp_variant   ~loc                  v.V.name  arg
      in
      pstr_value ~loc Nonrecursive
        [ value_binding ~loc ~pat:(pvar ~loc uncapitalized)
            ~expr:(List.fold_right (V.args v)
                     ~init:constructed_value
                     ~f:(fun (label,v) e ->
                       pexp_fun ~loc label None (pvar ~loc v) e)
            )
        ]
    in
    List.map variants ~f

  let matchings_exn loc (variants : V.t list) =
    let matching v =
      let uncapitalized = variant_name_to_string v.V.name in
      let match_exn_name = "get_" ^ uncapitalized ^ "_exn" in
      let failure =
        let str_constant = estring ~loc match_exn_name in
        [%expr raise (Failure [%e str_constant]) ]
      in
      let caser v' = match v'.V.kind with
        | `Normal params ->
          let l = List.length params in
          if l = 0 then (
            let lhs = ppat_construct ~loc (Located.lident ~loc v'.name) None in
            let rhs =
              if not @@ String.equal v.name v'.name
              then failure
              else eunit ~loc
            in                           
            case ~lhs ~guard:None ~rhs
          ) else (
            let names = List.init ~f:(fun i -> "x_" ^ (Int.to_string i)) l in
            let lhs =
              let args = ppat_tuple ~loc (List.map names ~f:(fun name -> pvar ~loc name)) in
              ppat_construct ~loc (Located.lident ~loc v'.name) (Some args)
            in
            let rhs =
              if not @@ String.equal v.name v'.name
              then failure
              else (
                let args = pexp_tuple ~loc (List.map names ~f:(evar ~loc)) in
                args
              )
            in
            case ~lhs ~guard:None ~rhs
          )
        | `Polymorphic param_opt ->
          match param_opt with
          | None -> (
            let lhs = ppat_variant ~loc v'.name None in
            let rhs =
              if not @@ String.equal v.name v'.name
              then failure
              else evar ~loc v.name
            in                           
            case ~lhs ~guard:None ~rhs
          )
          | Some _ -> (
            let lhs =
              ppat_variant ~loc v'.name (Some [%pat? x])
            in
            let rhs =
              if not @@ String.equal v.name v'.name
              then failure
              else [%expr x]
            in
            case ~lhs ~guard:None ~rhs
          )
      in
      let expr = pexp_function ~loc (List.map ~f:caser variants) in
      pstr_value ~loc Nonrecursive [
        value_binding ~loc ~pat:(pvar ~loc match_exn_name)
          ~expr
      ]
    in
    List.map variants ~f:matching

  let matchings_opt loc (variants : V.t list) =
    let none = [%expr None] in
    let some x = [%expr (Some ([%e x]))] in
    let matching v =
      let uncapitalized = variant_name_to_string v.V.name in
      let match_opt_name = "get_" ^ uncapitalized ^ "_opt" in
      let caser v' = match v'.V.kind with
        | `Normal params ->
          let l = List.length params in
          if l = 0 then (
            let lhs = ppat_construct ~loc (Located.lident ~loc v'.name) None in
            let rhs =
              if not @@ String.equal v.name v'.name
              then none
              else some (eunit ~loc)
            in                           
            case ~lhs ~guard:None ~rhs
          ) else (
            let names = List.init ~f:(fun i -> "x_" ^ (Int.to_string i)) l in
            let lhs =
              let args = ppat_tuple ~loc (List.map names ~f:(fun name -> pvar ~loc name)) in
              ppat_construct ~loc (Located.lident ~loc v'.name) (Some args)
            in
            let rhs =
              if not @@ String.equal v.name v'.name
              then none
              else (
                let args = pexp_tuple ~loc (List.map names ~f:(evar ~loc)) in
                some args
              )
            in
            case ~lhs ~guard:None ~rhs
          )
        | `Polymorphic param_opt ->
          match param_opt with
          | None -> (
            let lhs = ppat_variant ~loc v'.name None in
            let rhs =
              if not @@ String.equal v.name v'.name
              then none
              else some (evar ~loc v.name)
            in                           
            case ~lhs ~guard:None ~rhs
          )
          | Some _ -> (
            let lhs =
              ppat_variant ~loc v'.name (Some [%pat? x])
            in
            let rhs =
              if not @@ String.equal v.name v'.name
              then none
              else some [%expr x]
            in
            case ~lhs ~guard:None ~rhs
          )
      in
      let expr = pexp_function ~loc (List.map ~f:caser variants) in
      pstr_value ~loc Nonrecursive [
        value_binding ~loc ~pat:(pvar ~loc match_opt_name)
          ~expr
      ]
    in
    List.map variants ~f:matching

  let matchings_is loc (variants : V.t list) =
    let etrue = [%expr true] in
    let efalse = [%expr false] in
    let matching v =
      let uncapitalized = variant_name_to_string v.V.name in
      let match_opt_name = "is_" ^ uncapitalized in
      let caser v' = match v'.V.kind with
        | `Normal params ->
          let l = List.length params in
          if l = 0 then (
            let lhs = ppat_construct ~loc (Located.lident ~loc v'.name) None in
            let rhs =
              if not @@ String.equal v.name v'.name
              then efalse
              else etrue
            in                           
            case ~lhs ~guard:None ~rhs
          ) else (
            let names = List.init ~f:(fun i -> "x_" ^ (Int.to_string i)) l in
            let lhs =
              let args = ppat_tuple ~loc (List.map names ~f:(fun name -> pvar ~loc name)) in
              ppat_construct ~loc (Located.lident ~loc v'.name) (Some args)
            in
            let rhs =
              if not @@ String.equal v.name v'.name
              then efalse
              else etrue
            in
            case ~lhs ~guard:None ~rhs
          )
        | `Polymorphic param_opt ->
          match param_opt with
          | None -> (
            let lhs = ppat_variant ~loc v'.name None in
            let rhs =
              if not @@ String.equal v.name v'.name
              then efalse
              else etrue
            in                           
            case ~lhs ~guard:None ~rhs
          )
          | Some _ -> (
            let lhs =
              ppat_variant ~loc v'.name (Some [%pat? x])
            in
            let rhs =
              if not @@ String.equal v.name v'.name
              then efalse
              else etrue
            in
            case ~lhs ~guard:None ~rhs
          )
      in
      let expr = pexp_function ~loc (List.map ~f:caser variants) in
      pstr_value ~loc Nonrecursive [
        value_binding ~loc ~pat:(pvar ~loc match_opt_name)
          ~expr
      ]
    in
    List.map variants ~f:matching

  let variant loc ty =
    constructors loc ty

  let variants_of_td td =
    let loc = td.ptype_loc in
    variant loc (Inspect.type_decl td)

  let matchings_of_td td =
    let loc = td.ptype_loc in
    matchings_exn loc (Inspect.type_decl td) @
    matchings_is loc (Inspect.type_decl td) @
    matchings_opt loc (Inspect.type_decl td)
      
  let generate ~loc ~path:_ (rec_flag, tds) =
    (match rec_flag with
     | Nonrecursive ->
       Location.raise_errorf ~loc
         "nonrec is not compatible with the `ppx_variants_conv' preprocessor"
     | _ -> ());
    match tds with
    | [td] -> variants_of_td td @ matchings_of_td td
    | _ -> Location.raise_errorf ~loc "ppx_variants_conv: not supported"
end

let variants =
  Deriving.add "variants"
    ~str_type_decl:(Deriving.Generator.make_noarg Gen_str.generate)
    ~sig_type_decl:(Deriving.Generator.make_noarg Gen_sig.generate)
;;

